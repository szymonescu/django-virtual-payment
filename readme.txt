App written in python 3.8.5 using django 3.1 with sqlite as database backend.

* Run the server:
$ python manage.py runserver

* Run the data import:
$ python manage.py import_users_data <filename>

* Missing tests... :(