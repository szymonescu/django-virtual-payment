import csv

from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import F

from tokens.models import User, Token


class Command(BaseCommand):
    help = 'Imports users from external csv file'

    def add_arguments(self, parser):
        parser.add_argument('filename', nargs='?', type=str)

    def handle(self, *args, **options):
        users_data = self.extract_data(options['filename'])
        rewards_emails = []
        with transaction.atomic():
            for elem in users_data:
                if (referrer_email := elem['referrer_email']) == '':
                    elem['referrer_email'] = None
                else:
                    rewards_emails.append(referrer_email)
                self.create_user(elem)
            self.add_referral_points(rewards_emails)
        self.stdout.write(self.style.SUCCESS(f'Extracted data for {len(users_data)} users'))

    def extract_data(self, filename):
        with open(filename) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            # sort the list so that users with referrals are last which simplifies code later
            sorted_list = sorted(csv_reader, key=lambda row: row['referrer_email'])
            # according to my experience there should probably be some data validators here, as people tend to make
            # mistakes in such files, sometimes
        return sorted_list

    def create_user(self, elem):
        token, _ = self.get_token(elem)
        defaults = {'token': token, 'referrer_email': elem['referrer_email'] or None}
        defaults.update(elem)
        user, _ = User.objects.update_or_create(defaults=defaults, email=elem['email'])
        return user

    def get_token(self, data):
        return Token.objects.update_or_create(
            defaults={'balance': int(data.pop('balance'))}, user__email=data['email']
        )

    def add_referral_points(self, data):
        Token.objects.filter(user__email__in=data).update(balance=F('balance') + 20)

