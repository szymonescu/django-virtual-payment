from django.contrib import admin

# Register your models here.
from django.apps import apps


models = apps.get_app_config('tokens').get_models()

# models = apps.get_models()

for model in models:
    admin.site.register(model)
