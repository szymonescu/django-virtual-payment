from django.db import models


class User(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.TextField()
    last_name = models.TextField()
    email = models.TextField()
    token = models.OneToOneField('Token', on_delete=models.CASCADE)
    referrer_email = models.TextField(null=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}, {self.email}'


class Token(models.Model):
    source = models.ForeignKey('TokenSource', on_delete=models.SET_NULL, null=True)
    balance = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.balance}'


class TokenSource(models.Model):
    value = models.TextField()

    def __str__(self):
        return f'{self.value}'
