from django.shortcuts import render
from django.views.generic import ListView, DetailView

from tokens.models import User

# Following views could be unified to be DRY

class UserList(ListView):
    model = User
    template_name = 'user_list.html'

    def get(self, request, *args, **kwargs):
        context = {'users': self.model.objects.all()}
        return render(request, self.template_name, context)


class UserSingle(DetailView):
    model = User
    template_name = 'user_detail.html'

    def get(self, request, *args, **kwargs):
        context = {'user': self.model.objects.get(id=kwargs['user_id'])}
        return render(request, self.template_name, context)


# Missing view for amending user points
# This solution assumes that user can eithpick source name from drop down select menu on the website or input a new one
# as a string name.
# The method could look something like this:
#
# def amend_points(user_id, amount, source):
#     token_source, _ = TokenSource.objects.get_or_create(defaults={}, value=source)
#     Token.objects.filter(user_id=user_id).update(balance=F('balance') + amount, source=token_source)
